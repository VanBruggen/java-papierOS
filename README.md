# papierOS

Language: Polish.

A team project of a CLI OS simulator, written as part of the _Operating Systems_ subject on my CS studies. Although our responsibility was divided into separate modules by the professor, I've been deeply engaged in the development of the whole system, not just my module.

One can find my additional fix suggestions in a separate branch.

## Original contents of this README

#### Interfejs tekstowy wzorowany na Windowsie
Dominik Boła

#### Rozkazy asemblerowe i ich interpreter + 2 przykładowe programy
Mateusz Biernacki

#### Zarządzanie plikami i katalogami metodą indeksową
Grzegorz Piątkowski

#### Zarządzanie procesami wzorowane na Windowsie
Magda Ducka

#### Zarządzanie pamięcią operacyjną z użyciem stronicowania
Katarzyna Malowany

#### Mechanizmy synchronizacyjne wykorzystujące zamki (mutex locks)
Szymon Wiśniewski

#### Zarządzanie procesorem (planista) za pomocą algorytmu SRT
Patryk Miedziaszczyk